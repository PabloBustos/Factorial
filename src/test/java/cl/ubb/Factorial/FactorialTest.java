package cl.ubb.Factorial;

import static org.junit.Assert.*;

import org.junit.Test;



public class FactorialTest {

	@Test
	public void FactorialdeCeroRetornar1() {
		/*arrange*/
		Factorial fa = new Factorial();
		int Resultado;
		/*act*/
		Resultado=fa.DeterminarFactorial(0);
		/* assert */
		assertEquals(Resultado,1);
	}
	
	@Test
	public void FactorialdeUnoRetornar1() {
		/*arrange*/
		Factorial fa = new Factorial();
		int Resultado;
		/*act*/
		Resultado=fa.DeterminarFactorial(1);
		/* assert */
		assertEquals(Resultado,1);
	}
	
	@Test
	public void FactorialdeDosRetornar2() {
		/*arrange*/
		Factorial fa = new Factorial();
		int Resultado;
		/*act*/
		Resultado=fa.DeterminarFactorial(2);
		/* assert */
		assertEquals(Resultado,2);
	}
	
	@Test
	public void FactorialdeTresRetornar6() {
		/*arrange*/
		Factorial fa = new Factorial();
		int Resultado;
		/*act*/
		Resultado=fa.DeterminarFactorial(3);
		/* assert */
		assertEquals(Resultado,6);
	}
	
	@Test
	public void FactorialdeCuatroRetornar24() {
		/*arrange*/
		Factorial fa = new Factorial();
		int Resultado;
		/*act*/
		Resultado=fa.DeterminarFactorial(4);
		/* assert */
		assertEquals(Resultado,24);
	}
	
	@Test
	public void FactorialdeCincoRetornar120() {
		/*arrange*/
		Factorial fa = new Factorial();
		int Resultado;
		/*act*/
		Resultado=fa.DeterminarFactorial(5);
		/* assert */
		assertEquals(Resultado,120);
	}


}
