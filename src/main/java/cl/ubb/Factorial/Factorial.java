package cl.ubb.Factorial;

public class Factorial {
	
	public int DeterminarFactorial(int i){
		int j, factorial=1;
		if(i>=2){
			for(j=1;j<=i;j++){
				factorial=factorial*j;
			}
			return factorial;
		}
		return 1;
	}

}
